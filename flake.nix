{
  description = "Unterste Brücke Site";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    devshell.url = "github:numtide/devshell";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    devshell,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [devshell.overlays.default];
      };
    in {
      devShell = pkgs.devshell.mkShell {
        name = "unterste";
        packages = with pkgs; [
          python3
        ];
        commands = [
          {
            name = "serve";
            category = "develop";
            help = "Serve site";
            command = "${pkgs.python3}/bin/python3 -m http.server";
          }
        ];
      };
    });
}
